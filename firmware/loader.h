#ifndef _LOADER_H
#define _LOADER_H
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>
#include <string.h>
#include <bios.h>
#include "tusb.h"
#include "pico/stdlib.h"
#include "hardware/watchdog.h"
#include "pico/multicore.h"
#include "hardware/flash.h"
#include "hardware/exception.h"
#include "pico/unique_id.h"

#define VERSION "0.1.0"
#define FIRMWARE "dpwis"
#define FIRMWAREVER "00001"
/*
d----  Debug mode
-p---  Raspberry Pi Pico
--w--  Wireless
---i-  ILI9341 display
----s  microSD card
*/

__attribute__((__noinline__))
void *get_pc() { return __builtin_return_address(0); }

int main();
void chpmgr_entry();
void hardfault_handler();
void nmi_handler();
void svcall_handler();

__attribute__((naked))
void enter_usermode(void) {
  asm volatile(
    "mrs r0, control\n"
    "mov r1, #1\n"
    "orr r0, r1\n"
    "msr control, r0\n"
    "bx lr\n"
  );
}

uint32_t read_control_register(void) {
  uint32_t value;
  asm volatile("mrs %0, control" : "=r" (value));
  return value;
}

void syscall(uint32_t number) {
  asm volatile(
    "mov r12, %0\n"
    "svc #0\n"
    :
    : "r" (number)
    : "r12"
  );
}

void dumpregisters() {
  register uint32_t r0 asm ("r0");
  register uint32_t r1 asm ("r1");
  register uint32_t r2 asm ("r2");
  register uint32_t r3 asm ("r3");
  register uint32_t r4 asm ("r4");
  register uint32_t r5 asm ("r5");
  register uint32_t r6 asm ("r6");
  register uint32_t r7 asm ("r7");
  register uint32_t r8 asm ("r8");
  register uint32_t r9 asm ("r9");
  register uint32_t r10 asm ("r10");
  register uint32_t r11 asm ("r11");
  register uint32_t r12 asm ("r12");

  kprintf("r0: [%d %x]\n", r0, r0);
  kprintf("r1: [%d %x]\n", r1, r1);
  kprintf("r2: [%d %x]\n", r2, r2);
  kprintf("r3: [%d %x]\n", r3, r3);
  kprintf("r4: [%d %x]\n", r4, r4);
  kprintf("r5: [%d %x]\n", r5, r5);
  kprintf("r6: [%d %x]\n", r6, r6);
  kprintf("r7: [%d %x]\n", r7, r7);
  kprintf("r8: [%d %x]\n", r8, r8);
  kprintf("r9: [%d %x]\n", r9, r9);
  kprintf("r10: [%d %x]\n", r10, r10);
  kprintf("r11: [%d %x]\n", r11, r11);
  kprintf("r12: [%d %x]\n", r12, r12);
}

#define __get_MSP(x) asm volatile ("mrs %0, msp" : "=r" (x))
#define __get_PSP(x) asm volatile ("mrs %0, psp" : "=r" (x))

#endif
