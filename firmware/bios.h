#ifndef _BIOS_H
#define _BIOS_H

void kprintf(const char*, ...);
void kputs(const char*);
void kputc(const char);
void khexdump(unsigned char*, int);
void kfuncdump(unsigned char*, int);

#define PANIC(s) hchp_panic(s)
void hchp_panic(const char*);

#endif
