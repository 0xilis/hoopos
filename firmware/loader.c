#include <macho.h>
#include <loader.h>
#include <mpu.h>
#include <string.h>
#include "../bins/test.bin.h"
#include "bios.h"

struct {
  uint32_t pf_mark; // CAFEC9DE (cafecode)
  uint32_t pf_sig;  // 5AFEB007 (safeboot)
  uint32_t kp_base;
  uint32_t kp_len;
} kernelptr_pf = { 0xCAFEC0DE, 0x5AFEB007, 0, 0 };

// hoopOS loader entrypoint
// verify integrity and chainload kernel
int main() {
  stdio_usb_init();
  sleep_ms(1000);

  pico_unique_board_id_t sn;
  pico_get_unique_board_id(&sn);

  kputs("hoopOS " VERSION " " FIRMWARE FIRMWAREVER "\nSerial Number: ");
  for (int i = 0; i < 8; i++) {
    int n = sn.id[i];
    if (n < 26) {
      kprintf("%c", n + 'a');
    } else if (n > 229) {
      kprintf("%c", n - 230 + 'A');
    } else {
      kprintf("%03d", n);
    }
  }
  kprintf("\nloader | Address: %p\n", get_pc());
  sleep_ms(100);

  // TODO setup hardware
  kputs("loader | Configuring hardware\n");

  exception_set_exclusive_handler(HARDFAULT_EXCEPTION, hardfault_handler);
  exception_set_exclusive_handler(NMI_EXCEPTION, hardfault_handler);
  exception_set_exclusive_handler(SVCALL_EXCEPTION, svcall_handler);
  sleep_ms(100);

  // TODO enable memory protections
  kputs("loader | Enabling memory protections\n");
  if (mpu_region_set(0, 0x10000000, MPU_ATTR_NORMAL_WT | MPU_ATTR_AP_URO, MPU_SIZE_4G)) {
    kputs("loader | error: could not set MPU region 0\n");
  }

  if (mpu_region_set(2, 0x20000000, MPU_ATTR_NORMAL_WT | MPU_ATTR_AP_FULL, MPU_SIZE_4G)) {
    kputs("loader | error: could not set MPU region 2\n");
  }

  if (mpu_region_set(1, 0x40000000, MPU_ATTR_NORMAL_WT | MPU_ATTR_AP_FULL, MPU_SIZE_4G)) {
    kputs("loader | error: could not set MPU region 1\n");
  }

  if (mpu_enable()) {
    kputs("loader | error: could not enable MPU\n");
  }

  // launch chpmgr core
  kputs("loader | Launching chpmgr core\n");
  multicore_launch_core1(chpmgr_entry);
  sleep_ms(100);

  // TODO verify integrity
  kputs("loader | Verifying integrity\n");
  sleep_ms(100);

  // TODO chainload kernel
  kputs("loader | Chainloading kernel\n");
  sleep_ms(100);

  if (kernelptr_pf.kp_base == 0 || kernelptr_pf.kp_len == 0) {
    kputs("loader | error: Kernel could not be found, entering recovery...\n");
    //enter_recovery();
  }

  syscall(69);

  unsigned char *binmem = /*vm_alloc_proc_slice()*/ (unsigned char*)0x20010000;
  kputs("loader | Allocated memory for test binary\n");

  loadedbin_t *bin = macho_load(test_bin, test_bin_len, binmem);
  kprintf("loader | Main Mach-O: %p\n", bin);

  int (*entry)(char*) = macho_func(bin, "_start");
  kprintf("loader | Main Mach-O _start: %p\n", entry);

  kputs("_start\n");
  kfuncdump(macho_sym(bin, "_start"), 64);

  kputs("_function\n");
  kfuncdump(macho_sym(bin, "_function"), 64);

  kputs("reloc subroutine\n");
  khexdump(macho_sym(bin, "_function") + 0x8 + 0xc, 64);

  kprintf("0x2b0 reloc offset = %p\n", *((void**) ((char*) binmem + 0x2b0)));
  khexdump(binmem + 0x2b0, 4);

  //*((void**) macho_sym(bin, "_print")) = kputs;

  // for debugging later
  //asm volatile ("bkpt $0");

  const char *cmdline = "-v hmfi_get_out_of_my_way=0x1";

  kprintf("address of cmdline: %p\n", cmdline);

  khexdump(binmem + 0x2a0, 64);

  sleep_ms(100);
  entry(cmdline);

  //kputs("the _print global:\n");
  //khexdump(macho_sym(bin, "_print"), 4);

  kputs("\nloader | Entering usermode\n");
  sleep_ms(100);
  enter_usermode();

  kputs("loader | Hello from usermode!\n");
  sleep_ms(100);

  // wont get here
  PANIC("unreachable");
}

void chpmgr_entry() {
  int cpuid = sio_hw->cpuid;
  kprintf("chpmgr | Started chip manager on core %d\n", cpuid);
  while(1) {}
}

void hchp_panic_ex(const char *s, void *caller, void *lr) {
  int cpuid = sio_hw->cpuid;
  kprintf("\npanic(cpu %d caller %p): %s\n", cpuid, caller, s);
  kputc('\n');
  save_and_disable_interrupts();

  watchdog_enable(1, 1);
  while(1) {}
}

void hchp_panic(const char *s) {
  hchp_panic_ex(s, __builtin_return_address(0), 0);
}

void hardfault_handler() {
  register int frame, lr;

  asm volatile("mov %0, lr\n" : "=r" (lr));
  if ((lr & (1 << 2)) == 0) {
    __get_MSP(frame);
  } else {
    __get_PSP(frame);
  }

  hchp_panic_ex("Hard Fault occurred",
      (void*) *(uint32_t*) (frame + 0x18),
      (void*) *(uint32_t*) (frame + 0x14));
}

void nmi_handler() {
  register int frame, lr;

  asm volatile("mov %0, lr\n" : "=r" (lr));
  if ((lr & (1 << 2)) == 0) {
    __get_MSP(frame);
  } else {
    __get_PSP(frame);
  }

  hchp_panic_ex("NMI occurred", (void*) *(uint32_t*) (frame + 0x18), (void*) *(uint32_t*) (frame + 0x14));
}

void syscall_handler(uint32_t *frame) {
  uint32_t svcn = frame[4];
  kprintf("svcall | System call %p %d\n", frame, svcn);
}

__attribute__((naked))
void svcall_handler() {
  asm volatile(
    "push {r0, r1, lr}\n"
    "mov r0, lr\n"
    "mov r1, #4\n"
    "and r0, r1\n"
    "cmp r0, #0\n"
    "bne use_psp\n"
    "mrs r0, msp\n"
    "b end\n"
    "use_psp: mrs r0, psp\n"
    "end: sub r0, #4\n"
    "bl syscall_handler\n"
    "pop {r0, r1, pc}\n"
    : : : "r0", "r1"
  );
}

void kprintf(const char *fmt, ...) {
  static char __kprintfbuf[4096];
  va_list args;
  va_start(args, fmt);

  int size = vsnprintf(NULL, 0, fmt, args);
  char *buf = __kprintfbuf;
  if (size > 4095) { // if the buffer cant fit it, allocate a new one
    char *buf = malloc(size);

    vsprintf(buf, fmt, args);
    va_end(args);
    kputs(buf);

    free(buf);
    return;
  }

  vsprintf(buf, fmt, args);
  va_end(args);
  kputs(buf);
}

void kputs(const char *str) {
  tud_cdc_n_write(0, str, strlen(str));
  tud_cdc_n_write_flush(0);
}

void kputc(const char c) {
  tud_cdc_n_write_char(0, &c, 1);
  tud_cdc_n_write_flush(0);
}

void khexdump(unsigned char *data, int len) {
  for (int i = 0; i < len; i++) {
    kprintf("%02x ", data[i]);

    if ((i + 1) % 8 == 0)
      kputc(' ');

    if ((i + 1) % 16 == 0)
      kputc('\n');
  }
  if (len % 16 != 0)
    kputc('\n');
}
void kfuncdump(unsigned char *data, int len) {
  int a = 0, i;

  for (i = 0; i < len; i++) {
    if ((*data) == 0x80 && (*(data + 1)) == 0xb5) {
      a = 1;
      break;
    }
    data--;
  }

  if (!a) {
    kputs("<not found>\n");
    return;
  }

  for (i = 0; i < len; i++) {
    kprintf("%02x ", data[i]);

    if ((i + 1) % 8 == 0)
      kputc(' ');

    if ((i + 1) % 16 == 0)
      kputc('\n');

    if (data[i] == 0xbd && data[i -1] == 0x80)
      break;
  }
  if ((i + 1) % 16 != 0)
    kputc('\n');
}
