#include <mpu.h>
#include <stdint.h>

int mpu_enable() {
  volatile uint32_t *reg = (volatile uint32_t*) REG_MPU_CTRL;
  *reg |= MPU_CTRL_ENABLE | MPU_CTRL_PRIVDEFENA; // set ctrl enable & privdefena
  return !(*reg & MPU_CTRL_ENABLE);
}

int mpu_disable() {
  volatile uint32_t *reg = (volatile uint32_t*) REG_MPU_CTRL;
  *reg &= ~MPU_CTRL_ENABLE; // clear ctrl enable
  return *reg & MPU_CTRL_ENABLE;
}

int mpu_region_set(uint8_t region, uint32_t addr, uint16_t attrs, uint8_t size) {
  volatile uint32_t *reg = (volatile uint32_t*) REG_MPU_RNR;
  *reg = (*reg & 0xFFFFFFF0) | (region & 0x0F); // store rnr region

  reg = (volatile uint32_t*) REG_MPU_RBAR;
  uint32_t rbar = *reg;
  rbar &= ~MPU_RBAR_VALID; // clear rbar valid
  rbar &= ~(0xFFFFFF << 8); // clear rbar address base
  rbar |= (addr & 0xFFFFFF) << 8; // store rbar address base
  *reg = rbar;

  reg = (volatile uint32_t*) REG_MPU_RASR;
  uint32_t rasr = *reg;
  rasr |= MPU_RASR_ENABLE; // set rasr enable
  rasr &= ~(0xFFFF << 16); // clear rasr attrs
  rasr |= (attrs & 0xFFFF) << 16; // store rasr attrs
  rasr &= ~(0xFF << 8); // clear rasr srd
  rasr &= ~(0x1F << 1); // clear rasr size
  rasr |= (size & 0x1F) << 1; // store rasr size

  *reg = rasr;
  return !(*reg & MPU_RASR_ENABLE);
}

uint32_t mpu_region_get(uint8_t region) {
  volatile uint32_t *reg = (volatile uint32_t*) REG_MPU_RNR;
  *reg = (*reg & 0xFFFFFFF0) | (region & 0x0F); // store rnr region

  reg = (volatile uint32_t*) REG_MPU_RASR;
  return *reg;
}

int mpu_region_clear(uint8_t region) {
  volatile uint32_t *reg = (volatile uint32_t*) REG_MPU_RNR;
  *reg = (*reg & 0xFFFFFFF0) | (region & 0x0F); // store rnr region

  reg = (volatile uint32_t*) REG_MPU_RASR;
  *reg &= ~(1 << 0); // clear rasr enable

  return *reg & (1 << 0);
}

