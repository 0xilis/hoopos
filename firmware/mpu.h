#ifndef _MPU_H
#define _MPU_H
#include <stdint.h>

#define REG_MPU_TYPE 0xE000ED90
#define REG_MPU_CTRL 0xE000ED94
#define REG_MPU_RNR 0xE000ED98
#define REG_MPU_RBAR 0xE000ED9C
#define REG_MPU_RASR 0xE000EDA0

#define MPU_CTRL_ENABLE (1 << 0)
#define MPU_CTRL_HFNMIENA (1 << 1)
#define MPU_CTRL_PRIVDEFENA (1 << 2)
#define MPU_RBAR_VALID (1 << 4)
#define MPU_RASR_ENABLE (1 << 0)

#define MPU_ATTR_STRONGLY_ORDERED (0)
#define MPU_ATTR_DEVICE (1 << 0)
#define MPU_ATTR_NORMAL_WT (1 << 1)
#define MPU_ATTR_NORMAL_WB ((1 << 0) | (1 << 1))
#define MPU_ATTR_SHAREABLE (1 << 2)
#define MPU_ATTR_AP_NONE (0)
#define MPU_ATTR_AP_PRIV (1 << 8)
#define MPU_ATTR_AP_URO (1 << 9)
#define MPU_ATTR_AP_FULL ((1 << 8) | (1 << 9))
#define MPU_ATTR_AP_PRO ((1 << 8) | (1 << 10))
#define MPU_ATTR_AP_RO ((1 << 9) | (1 << 10))
#define MPU_ATTR_EXECUTE_NEVER (1 << 12)

#define MPU_SIZE_256B 0x07U
#define MPU_SIZE_512B 0x08U
#define MPU_SIZE_1K 0x09U
#define MPU_SIZE_2K 0x0AU
#define MPU_SIZE_4K 0x0BU
#define MPU_SIZE_8K 0x0CU
#define MPU_SIZE_16K 0x0DU
#define MPU_SIZE_32K 0x0EU
#define MPU_SIZE_64K 0x0FU
#define MPU_SIZE_128K 0x10U
#define MPU_SIZE_256K 0x11U
#define MPU_SIZE_512K 0x12U
#define MPU_SIZE_1M 0x13U
#define MPU_SIZE_2M 0x14U
#define MPU_SIZE_4M 0x15U
#define MPU_SIZE_8M 0x16U
#define MPU_SIZE_16M 0x17U
#define MPU_SIZE_32M 0x18U
#define MPU_SIZE_64M 0x19U
#define MPU_SIZE_128M 0x1AU
#define MPU_SIZE_256M 0x1BU
#define MPU_SIZE_512M 0x1CU
#define MPU_SIZE_1G 0x1DU
#define MPU_SIZE_2G 0x1EU
#define MPU_SIZE_4G 0x1FU

int mpu_enable();
int mpu_disable();

int mpu_region_set(uint8_t region, uint32_t base, uint16_t attrs, uint8_t size);
uint32_t mpu_region_get(uint8_t region);
int mpu_region_clear(uint8_t region);

#endif
