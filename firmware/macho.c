#include "bios.h"
#include <macho.h>

// TODO implement hold_resolve_symbol
void *hold_resolve_symbol(const char *name) {
  kprintf("  hold | Resolving symbol '%s' -> %p\n", name, kputs);
  khexdump(kputs, 64);
  return kputs;
}

// TODO implement hold_link_exports
void hold_link_exports(export_t *exports) {
  kputs("  hold | Linking exports\n");

  while (exports->name != NULL) {
    kprintf("         Export '%s'\n", exports->name);
    exports = &exports[1];
    if (!exports->name && exports->value) {
      exports = exports->value;
    }
  }
}

uint32_t reverse_bytes(uint32_t bytes) {
  uint32_t aux = 0;
  uint8_t byte;

  for (int i = 0; i < 32; i += 8) {
      byte = (bytes >> i) & 0xff;
      aux |= byte << (32 - 8 - i);
  }
  return aux;
}

loadedbin_t *macho_load(const unsigned char *data, size_t length, unsigned char *base) {
  loadedbin_t *bin = malloc(sizeof(loadedbin_t));
  memset(bin, 0, sizeof(loadedbin_t));

  bin->header = (macho_header_t*) data;
  bin->length = length;
  bin->base = base;

  if (bin->header->magic != MACHO_MAGIC32) {
    kputs(" macho | Invalid magic\n");
    return NULL;
  }

  if (bin->header->cputype != MACHO_CPU_ARM) {
    kputs(" macho | Invalid CPU type\n");
    return NULL;
  }

  if (bin->header->cpusubtype != MACHO_CPUSUB_ARVMV6 && bin->header->cpusubtype != MACHO_CPUSUB_ARVMV6M && bin->header->cpusubtype != MACHO_CPUSUB_ALL) {
    kputs(" macho | Invalid CPU subtype\n");
    return NULL;
  }

  uint32_t base_vmaddr = -1;
  uint32_t vmsz_needed = 0;

  kputs(" macho | Reading load commands\n");

  int offset = sizeof(macho_header_t);
  for (int i = 0; i < bin->header->ncmds; i++) {
    macho_loadcmd_t *load = (macho_loadcmd_t*) ((char*) bin->header + offset);
    uint32_t type = load->cmd & ~MACHO_LOAD_REQUIRED;

    kprintf("         Segment type 0x%x size 0x%x\n", type, load->cmdsize);

    if (type == MACHO_LOADSEG32) {
      macho_loadseg32_t *loadseg32 = (macho_loadseg32_t*) load;
      if (loadseg32->vmaddr < base_vmaddr)
        base_vmaddr = loadseg32->vmaddr;
      if (loadseg32->vmaddr + loadseg32->vmsize > vmsz_needed)
        vmsz_needed = loadseg32->vmaddr + loadseg32->vmsize;
    } else if (type == MACHO_LOADSYMTAB) {
      bin->symtab = (macho_loadsymtab_t*) load;
    } else if (type == MACHO_LOADDYSYMTAB) {
      bin->dysymtab = (macho_loaddysymtab_t*) load;
    }

    offset += load->cmdsize;
  }

  kputs(" macho | Reading segments\n");
  offset = sizeof(macho_header_t);
  for (int i = 0; i < bin->header->ncmds; i++) {
    macho_loadcmd_t *load = (macho_loadcmd_t*) ((char*) bin->header + offset);
    uint32_t type = load->cmd & ~MACHO_LOAD_REQUIRED;

    if (type == MACHO_LOADSEG32) {
      macho_loadseg32_t *loadseg32 = (macho_loadseg32_t*) load;
      kprintf(" macho | Load Segment '%s'\n", loadseg32->segname);
      kprintf("         VM Address %p\n", loadseg32->vmaddr);
      kprintf("         VM Size %d\n", loadseg32->vmsize);
      kprintf("         Section Count %d\n", loadseg32->nsects);

      memset(base + loadseg32->vmaddr - base_vmaddr, 0, loadseg32->vmsize);
      memcpy(base + loadseg32->vmaddr - base_vmaddr, data + loadseg32->fileoff, loadseg32->filesize);

      macho_segsect32_t *sections = (macho_segsect32_t*) ((char*) loadseg32 + sizeof(macho_loadseg32_t));
      for (int j = 0; j < loadseg32->nsects; j++) {
        macho_segsect32_t sect = sections[j];
        kprintf("         Section %d '%s'\n", j, sect.sectname);
        kprintf("");
      }
    }

    offset += load->cmdsize;
  }

  kputs(" macho | Finding relocation symbols\n");
  const struct relocation_info *extrel = (void*) ((uintptr_t) bin->header + bin->dysymtab->extreloff);
  const struct relocation_info *locrel = (void*) ((uintptr_t) bin->header + bin->dysymtab->locreloff);
  const struct nlist *nlist = (struct nlist*) ((uintptr_t) bin->header + bin->symtab->symoff);

  kprintf("         extrel: %p\n", extrel);
  kprintf("         locrel: %p\n", locrel);
  kprintf("         nlist: %p\n", nlist);

  for (uint32_t sym_idx = 0; sym_idx < bin->symtab->nsyms; sym_idx++) {
    const struct nlist *nl = &nlist[sym_idx];
    uint32_t strx = nl->n_un.n_strx;
    const char *name = (const char *)((uintptr_t)bin->header + bin->symtab->stroff + strx);

    kprintf(" macho | Found symbol '%s'\n", name);

    int cmp = strcmp(name, "_module_entry");
    if (cmp == 0) {
      bin->entry = (void*) (nl->n_value + (uint32_t)base);
    }
    cmp = strcmp(name, "_exported_symbols");
    if (cmp == 0 && !bin->exports) {
      bin->exports = (export_t*)(nl->n_value + (uint32_t)base);
    }
  }

  kprintf(" macho | Relocating %d externals\n", bin->dysymtab->nextrel);
  for (uint32_t extrel_idx = 0; extrel_idx < bin->dysymtab->nextrel; extrel_idx++) {
    const struct relocation_info *ri = &extrel[extrel_idx];
    if (!ri->r_extern || ri->r_length != 2) {
      continue;
    }
    const struct nlist *nl = &nlist[ri->r_symbolnum];
    uint32_t strx = nl->n_un.n_strx;
    const char *name = (const char*) ((uintptr_t) bin->header + bin->symtab->stroff + strx);

    kprintf(" macho | Relocating symbol '%s'\n", name);

    void* symbol_value = hold_resolve_symbol(name);
    if (symbol_value == 0) {
      kputs(" macho | Linking failed\n");
      return NULL;
    }

    uint32_t vmoff = ri->r_address;
    kprintf(" macho | Relocating %p (base + %p) -> %p\n", (uint32_t*) ((char*) base + vmoff), vmoff, symbol_value);
    *(uint32_t*) ((char*) base + vmoff) = (uint32_t) symbol_value;
  }

  kprintf(" macho | Relocating %d locals\n", bin->dysymtab->nlocrel);
  for (uint32_t locrel_idx = 0; locrel_idx < bin->dysymtab->nlocrel; locrel_idx++) {
    const struct relocation_info *ri = &locrel[locrel_idx];
    if (ri->r_extern || ri->r_length != 2) {
      continue;
    }
    // Find the offset of the relocation pointer in the virtually mapped Mach-O and
    // slide it to the new base address.
    uint32_t vmoff = ri->r_address;
    uint32_t *reloc_ptr = (uint32_t*) ((uintptr_t) base + vmoff);
    *reloc_ptr = *reloc_ptr - base_vmaddr + ((uint32_t) base);
  }

  if (bin->exports) {
    hold_link_exports(bin->exports);
  }

  return bin;
}

void *macho_entry(loadedbin_t *bin) {
  return bin->entry;
}

void *macho_sym(loadedbin_t *bin, const char *name) {
  const struct nlist *nlist = (struct nlist*) ((uintptr_t) bin->header + bin->symtab->symoff);

  for (uint32_t sym_idx = 0; sym_idx < bin->symtab->nsyms; sym_idx++) {
    const struct nlist *nl = &nlist[sym_idx];
    uint32_t strx = nl->n_un.n_strx;
    const char *symname = (const char *)((uintptr_t)bin->header + bin->symtab->stroff + strx);

    if (strcmp(symname, name) == 0) {
      return (void*) (nl->n_value + (uint32_t)bin->base);
    }
  }

  return NULL;
}

void *macho_func(loadedbin_t *bin, const char *name) {
  void *func;
  if ((func = macho_sym(bin, name))) {
    return func + 1;
  }
  return NULL;
}
