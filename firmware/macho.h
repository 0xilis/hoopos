#ifndef _ELF_H
#define _ELF_H
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <bios.h>

#define MACHO_MAGIC32 0xfeedface
#define MACHO_CPU_ARM 0xc
#define MACHO_CPUSUB_ALL 0x0
#define MACHO_CPUSUB_ARVMV6 0x6
#define MACHO_CPUSUB_ARVMV6M 0xE
#define MACHO_LOAD_REQUIRED 0x80000000

typedef struct __attribute__((__packed__)) {
  const char *name;
  void *value;
} export_t;

typedef struct __attribute__((__packed__)) {
  char sectname[16];
  char segname[16];
  uint32_t addr;
  uint32_t size;
  uint32_t offset;
  uint32_t align;
  uint32_t reloff;
  uint32_t nreloc;
  uint32_t flags;
  uint32_t reserved1;
  uint32_t reserved2;
} macho_segsect32_t;

typedef struct __attribute__((__packed__)) {
  uint32_t cmd;
  uint32_t cmdsize;
} macho_loadcmd_t;

typedef struct __attribute__((__packed__)) {
  uint32_t cmd;
  uint32_t cmdsize;
  char segname[16];
  uint32_t vmaddr;
  uint32_t vmsize;
  uint32_t fileoff;
  uint32_t filesize;
  uint32_t maxprot;
  uint32_t initprot;
  uint32_t nsects;
  uint32_t flags;
} macho_loadseg32_t;
#define MACHO_LOADSEG32 0x1

typedef struct __attribute__((__packed__)) {
  uint32_t cmd;
  uint32_t cmdsize;
  uint32_t stroff;
  uint32_t timestamp;
  uint32_t current_version;
  uint32_t compatibility_version;
} macho_loadlinklib_t;
#define MACHO_LOADLINKLIB_FULL 0xc
#define MACHO_LOADLINKLIB_REL 0xd
#define MACHO_LOADLINKLIB_WEAK 0x18

typedef struct __attribute__((__packed__)) {
  uint32_t cmd;
  uint32_t cmdsize;
  uint32_t symoff;
  uint32_t nsyms;
  uint32_t stroff;
  uint32_t strsize;
} macho_loadsymtab_t;
#define MACHO_LOADSYMTAB 0x2

typedef struct __attribute__((__packed__)) {
  uint32_t cmd;
  uint32_t cmdsize;
  uint32_t ilocalsym;
  uint32_t nlocalsym;
  uint32_t iextrdefsym;
  uint32_t nextdefsym;
  uint32_t iundefsym;
  uint32_t nundefsym;
  uint32_t tocoff;
  uint32_t ntoc;
  uint32_t modtaboff;
  uint32_t nmodtab;
  uint32_t extremsymoff;
  uint32_t nextrefsyms;
  uint32_t indirectsymoff;
  uint32_t nindirectsyms;
  uint32_t extreloff;
  uint32_t nextrel;
  uint32_t locreloff;
  uint32_t nlocrel;
} macho_loaddysymtab_t;
#define MACHO_LOADDYSYMTAB 0xb

typedef struct __attribute__((__packed__)) {
  uint32_t cmd;
  uint32_t cmdsize;
  uint32_t rebfileoff;
  uint32_t rebsz;
  uint32_t bndfileoff;
  uint32_t bndsz;
  uint32_t wbndfileoff;
  uint32_t wbndsz;
  uint32_t lbndfileoff;
  uint32_t lbndsz;
  uint32_t expfileoff;
  uint32_t expsz;
} macho_loadcomptab_t;
#define MACHO_LOADCOMPTAB 0x22

typedef struct __attribute__((__packed__)) {
  uint32_t cmd;
  uint32_t cmdsize;
  uint64_t addr;
  uint64_t stacksize;
} macho_loadentry_t;
#define MACHO_LOADENTRY 0x28

typedef struct __attribute__((__packed__)) {
  uint32_t cmd;
  uint32_t cmdsize;
  unsigned char uuid[16];
} macho_loaduuid_t;
#define MACHO_LOADUUID 0x1b

typedef struct __attribute__((__packed__)) {
  uint32_t cmd;
  uint32_t cmdsize;
  uint32_t platform;
  uint32_t minosver;
  uint32_t sdkver;
  uint32_t ntools;
} macho_loadminosver_t;
#define MACHO_LOADMINOSVER 0x32

typedef struct __attribute__((__packed__)) {
  uint32_t magic;
  uint32_t cputype;
  uint32_t cpusubtype;
  uint32_t filetype;
  uint32_t ncmds;
  uint32_t sizeofcmds;
  uint32_t flags;
} macho_header_t;

typedef struct {
  macho_header_t *header;
  size_t length;

  unsigned char *base;

  void *entry;
  macho_loadsymtab_t *symtab;
  macho_loaddysymtab_t *dysymtab;

  export_t *exports;
} loadedbin_t;

// The following structures (nlist, relocation_info) are under the Apple Public Source License Version 2.0 (http://www.opensource.apple.com/apsl/)
// Copyright (c) 1999-2003 Apple Computer, Inc.  All Rights Reserved.
struct nlist {
  union {
    uint32_t n_strx;  /* index into the string table */
  } n_un;
  uint8_t n_type;     /* type flag, see below */
  uint8_t n_sect;     /* section number or NO_SECT */
  int16_t n_desc;     /* see <mach-o/stab.h> */
  uint32_t n_value;   /* value of this symbol (or stab offset) */
};
struct relocation_info {
  int32_t r_address;        /* offset in the section to what is being relocated */
  uint32_t r_symbolnum: 24, /* symbol index if r_extern == 1 or section ordinal if r_extern == 0 */
  r_pcrel: 1,               /* was relocated pc relative already */
  r_length: 2,              /* 0=byte, 1=word, 2=long, 3=quad */
  r_extern: 1,              /* does not include value of sym referenced */
  r_type: 4;                /* if not 0, machine specific relocation type */
};

loadedbin_t *macho_load(const unsigned char *data, size_t length, unsigned char *base);
void *macho_entry(loadedbin_t *bin);
void *macho_sym(loadedbin_t *bin, const char *name);
void *macho_func(loadedbin_t *bin, const char *name);

#endif
