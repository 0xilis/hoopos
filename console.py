#!/usr/bin/env python3

import serial
import time
import re

# !!! CHANGE THIS !!!
DEVICE='/dev/tty.usbmodem112401'
USECOLORS=True
printwait=True

def log(*args, **kwargs):
    print('[info]', *args, **kwargs)

def shsh(s):
    n = 13
    for c in s:
        n += ord(c)
        n *= 5
        n = n >> 3
    return n

def colorize(s):
    if not USECOLORS:
        return s

    match = re.match(r' *(\S*) *\|[\s\S]*', s)
    COLS = [32, 33, 34, 35]

    if s.startswith('panic('):
        return f"\033[0;31m{s}\033[0m"
    elif s.startswith('hoopOS ') or s.startswith('Serial N'):
        return f"\033[0;36m{s}\033[0m"
    elif match:
        col = COLS[shsh(match.group(1)) % len(COLS)]
        return f"\033[0;{col}m{s}\033[0m"
    return f"\033[0;37m{s}\033[0m"

def readser(s):
    resp = s.read_until(b'\n')
    if len(resp) > 0:
        try:
            print(colorize(resp.decode()), end='')
        except UnicodeDecodeError:
            print(resp)

# open a serial connection
while True:
    if printwait:
        log('waiting for serial device...')
        printwait = False
    try:
        s = serial.Serial(DEVICE, 115200, timeout=1)
        log('serial device found!')

        printwait = True
        try:
            while True:
                readser(s)
        except KeyboardInterrupt:
            exit(0)
    except Exception as e:
        s = str(e)
        if 'no such file or directory' not in s.lower() and 'not configured' not in s.lower():
            print(e)
        time.sleep(0.1)



