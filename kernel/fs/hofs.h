#ifndef HOOP_HOFS_H
#define HOOP_HOFS_H
#include <stdint.h>
#include <stddef.h>

#define HOOP_HOFS_MAGIC       0x1d3f5a7c
#define HOOP_HOFS_BLOCK_SIZE  4096

#define HOOP_HOFS_FLAG_RDONLY    1 << 0
#define HOOP_HOFS_FLAG_PASSCODE  1 << 1
#define HOOP_HOFS_FLAG_UNLOCK0   1 << 2
#define HOOP_HOFS_FLAG_UNLOCK1   1 << 3
#define HOOP_HOFS_FLAG_UNLOCK2   1 << 4
#define HOOP_HOFS_FLAG_SEALED    1 << 5

#define HOOP_HOFS_TYPE_FILE  1 << 0
#define HOOP_HOFS_TYPE_DIR   1 << 1
#define HOOP_HOFS_TYPE_LINK  1 << 2
#define HOOP_HOFS_TYPE_PR    1 << 3
#define HOOP_HOFS_TYPE_PW    1 << 4
#define HOOP_HOFS_TYPE_PX    1 << 5
#define HOOP_HOFS_TYPE_PRX   (1 << 3) | (1 << 5)
#define HOOP_HOFS_TYPE_PRW   (1 << 3) | (1 << 4)
#define HOOP_HOFS_TYPE_PRWX  (1 << 3) | (1 << 4) | (1 << 5)


typedef struct __attribute__((packed)) hoop_hofs_superblock {
  uint32_t  magic;
  uint64_t  chip_enc;
  uint64_t  passcode_enc;
  uint64_t  unlock_enc0;
  uint64_t  unlock_enc1;
  uint64_t  unlock_enc2;
  uint32_t  flags;
  uint32_t  nblocks;
  uint32_t  ninodes;
  uint8_t   seal[8];
  uint32_t  freeblocks[16];
} hoop_hofs_superblock;

typedef struct __attribute__((packed)) hoop_hofs_inode {
  uint32_t  inode;
  uint8_t   type;
  char      name[79];
  uint32_t  size;
  uint64_t  file_enc;
  uint32_t  direct[7];
  uint32_t  indirect;
} hoop_hofs_inode;

uint64_t hoop_hofs_enc_hash(uint64_t base, uint8_t *src, size_t len);
uint64_t hoop_hofs_enc_merge(uint64_t a, uint64_t b);
uint8_t *hoop_hofs_enc_encrypt(uint64_t key, uint32_t block, uint8_t *buf, size_t len);

#endif
