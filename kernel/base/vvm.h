#ifndef HOOP_VVM_H
#define HOOP_VVM_H
#include <stdint.h>

// 256k from 0x20000000 to 0x20040000
// 64 pages (4k)
// 16 slices (16k)

#define HOOP_PAGE_SIZE   4096
#define HOOP_STACK_SIZE  4096
#define HOOP_SLICE_SIZE  16384

#endif
